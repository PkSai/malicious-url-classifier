# Malicious URL Classifier

![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/logo.jpeg)

A Malicious URL Classifier that can classify URLs as malicious based on certain features.

# Dataset
A dataset consisting of 651,191 URLs out of which 428,103 benign and 96,457 defacement URLs, 94,111 phishing URLs and 32,520 malware URLs.
The dataset is split into 80-20 ratio.

Each URL is broken down based on protocol, subdomain, domain, top level domain, path, file extension, parameters. These different arguments will make up the feature set to be used in classification.

![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/1.png)

# Vectorization
## Count Vectorization
CountVectorizer converts a collection of text documents into a matrix where the rows represent the documents, and the columns represent the tokens (words or n-grams). It counts the occurrences of each token in each document, creating a “document-term matrix” with integer values representing the frequency of each token.

![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/count.jpg)

## TF-IDF
tf-idf is a weighting system that assigns a weight to each word in a document based on its term frequency (tf) and the reciprocal document frequency (tf) (idf). The words with higher scores of weight are deemed to be more significant.

![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/tfidf.png)

# Top features obtained

![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/2.png)

# Classifiers and Classification using various models

**A comparison between models**:
## 1. Multinomial Logistic regression with TF-ID
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/3.png)
## 2. Multinomial Logistic regression with Count Vectorizer
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/4.png)
## 3. Multinomial Naive Bayesian with TF-IDF
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/5.png)
## 4. Multinomial Naive Bayesian with Count Vectorizer
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/6.png)
## 5. AdaBoost/Decision Tree with TF-IDF
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/7.png)
## 6. AdaBoost/Decision Tree with Count Vectorizer
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/8.png)
## 7. Decision Tree with TF-IDF
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/9.png)
## 8. Decision Tree with Count Vectorizer
## ![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/10.png)
## 9. Random Forest with TF-IDF
![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/11.png)
## 10. Random Forest with Count Vectorizer
![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/12.png)


# Final Comparison of Classifiers
![image](https://gitlab.com/PkSai/malicious-url-classifier/-/raw/main/img/13.png)

# %% [code]
# Detection of Malicious URLs using machine learning
**A comparison between models**:
1. Multinomial Logistic regression with TF-ID
2. Multinomial Logistic regression with Count Vectorizer
3. Multinomial Naive Bayesian with TF-IDF
4. Multinomial Naive Bayesian with Count Vectorizer
5. AdaBoost/Decision Tree with TF-IDF
6. AdaBoost/Decision Tree with Count Vectorizer
7. Decision Tree with TF-IDF
8. Decision Tree with Count Vectorizer
9. Random Forest with TF-IDF
10. Random Forest with Count Vectorizer

# %% [markdown]
# ## Importing required libraries

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:39:40.179073Z","iopub.execute_input":"2022-05-30T13:39:40.179784Z","iopub.status.idle":"2022-05-30T13:39:41.552608Z","shell.execute_reply.started":"2022-05-30T13:39:40.179687Z","shell.execute_reply":"2022-05-30T13:39:41.551747Z"}}
import numpy as np 
import pandas as pd 
import random
import matplotlib.pyplot as plt

import os
for dirname, _, filenames in os.walk('/kaggle/input'):
    for filename in filenames:
        print(os.path.join(dirname, filename))

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, ExtraTreesClassifier

# Import Scikit-learn metric functions
from sklearn.metrics import confusion_matrix, classification_report
import seaborn as sns

from sklearn.model_selection import train_test_split

# %% [markdown]
# ## Loading the dataset

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:39:41.554125Z","iopub.execute_input":"2022-05-30T13:39:41.554370Z","iopub.status.idle":"2022-05-30T13:39:42.933348Z","shell.execute_reply.started":"2022-05-30T13:39:41.554343Z","shell.execute_reply":"2022-05-30T13:39:42.932375Z"}}
urls_data  = pd.read_csv("../input/malicious/malicious_phish.csv")
print("\nCSV data has been loaded...\n")

# %% [markdown]
# ## Listing out some entries of the Data

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:39:42.934585Z","iopub.execute_input":"2022-05-30T13:39:42.934822Z","iopub.status.idle":"2022-05-30T13:39:42.956016Z","shell.execute_reply.started":"2022-05-30T13:39:42.934794Z","shell.execute_reply":"2022-05-30T13:39:42.955390Z"}}
type(urls_data)
print("\nOverview of the entries")
print("\n==================")
urls_data.head()
urls_data

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:39:42.957863Z","iopub.execute_input":"2022-05-30T13:39:42.958352Z","iopub.status.idle":"2022-05-30T13:39:43.257187Z","shell.execute_reply.started":"2022-05-30T13:39:42.958316Z","shell.execute_reply":"2022-05-30T13:39:43.256314Z"}}
count = urls_data.type.value_counts()
count
sns.barplot(x=count.index, y=count, palette="Blues_d")
plt.xlabel('Types')
plt.ylabel('Count');

# %% [markdown]
# ## A token generator
# This function generates the tokens, each segment is delimited by a special character ( a slash, a dot, etc).

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:39:43.258578Z","iopub.execute_input":"2022-05-30T13:39:43.258828Z","iopub.status.idle":"2022-05-30T13:39:43.269600Z","shell.execute_reply.started":"2022-05-30T13:39:43.258798Z","shell.execute_reply":"2022-05-30T13:39:43.268160Z"}}
def genTokens(f):
    BySlash_Tokens = str(f.encode('utf-8')).split('/')
    total_Tokens = []
    for i in BySlash_Tokens:
        tokens = str(i).split('-')
        ByDot_Tokens = []
        for j in range(0,len(tokens)):
            temp_Tokens = str(tokens[j]).split('.')
            ByDot_Tokens += temp_Tokens
        total_Tokens = total_Tokens + tokens + ByDot_Tokens
    total_Tokens = list(set(total_Tokens))
Tvectorizer = TfidfVectorizer(tokenizer=genTokens, max_features=4000)
X_tv = Tvectorizer.fit_transform(url_list)
Cvectorizer = CountVectorizer(tokenizer=genTokens, max_features=4000)
X_cv = Cvectorizer.fit_transform(url_list)

def genHeatmap(cmatrix, score, creport):

  plt.figure(figsize=(6,6))
  sns.heatmap(cmatrix, 
              annot=True, 
              fmt=".1f", 
              linewidths=1.5, 
              #square = True, 
              cmap = 'BuPu_r', 
              annot_kws={"size": 16}, 
              xticklabels=['benign', 'defacement', 'phising','malware'],
              yticklabels=['benign', 'defacement', 'phising','malware'])
  plt.xticks(rotation='horizontal', fontsize=10)
  plt.yticks(rotation='horizontal', fontsize=10)
  plt.xlabel('Actual', size=15);
  plt.ylabel('Predicted', size=15);
  title = 'Accuracy: {0:.4f}'.format(score)
  plt.title(title, size = 15);
  print(creport)
  plt.show()

# 9. Random Forest with TF-IDF
# 10. Random Forest with Count Vectorizer

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:41:31.024683Z","iopub.execute_input":"2022-05-30T13:41:31.024985Z","iopub.status.idle":"2022-05-30T13:42:16.278665Z","shell.execute_reply.started":"2022-05-30T13:41:31.024947Z","shell.execute_reply":"2022-05-30T13:42:16.277798Z"}}
# Logistic Regression TF-IDF Vectorizer
lgtf_model = LogisticRegression(solver='liblinear')
lgtf_model.fit(X_train_tv, y_train_tv)


# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:53:02.263580Z","iopub.execute_input":"2022-05-30T13:53:02.263867Z","iopub.status.idle":"2022-05-30T13:55:53.923274Z","shell.execute_reply.started":"2022-05-30T13:53:02.263825Z","shell.execute_reply":"2022-05-30T13:55:53.922412Z"}}
# Decision Tree classifier
# %% [markdown]
# ## Results
# Comparison between the models

# %% [code] {"jupyter":{"outputs_hidden":false},"execution":{"iopub.status.busy":"2022-05-30T13:59:46.311155Z","iopub.execute_input":"2022-05-30T13:59:46.311477Z","iopub.status.idle":"2022-05-30T13:59:46.577901Z","shell.execute_reply.started":"2022-05-30T13:59:46.311437Z","shell.execute_reply":"2022-05-30T13:59:46.577006Z"}}
print("\nAccuracy of the Models")
print("========================")
plt.figure(figsize=(10, 5))
Model = ["LGTF", "LGCV", "NBTF", "NBCV", "ABTF", "ABCV", "DTF", "DTC", "RFF", "RFC"]
Accuracy = [score_lgtf, score_lgcv, score_mtf, score_mcv, score_atf, score_acv, score_dtf, score_dtv, score_rff, score_rfv]
plots = sns.barplot(x=Model, y=Accuracy, palette="BuPu_r")
for bar in plots.patches:
    plots.annotate(format(bar.get_height(), '.2f'),
                   (bar.get_x() + bar.get_width() / 2,
                    bar.get_height()), ha='center', va='center',
                   size=15, xytext=(0, 8),
                   textcoords='offset points')

plt.xlabel("Models", size=14)
plt.ylabel("Accuracy", size=14)
plt.show()


#sns.barplot(x=Models, y=Score, palette="Blues_d")
#plt.xlabel('Types')
#plt.ylabel('Count');
